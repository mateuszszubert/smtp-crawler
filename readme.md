# smtp-crawler
This is automated scriptology for finding mail addresses which has to exist, but they are not meant to be revealed offically.
# Why?
 - for finding addresses of people which prefers to not receive messagess directly, but from secretary unit
 - ...why not?
# How it works?
Based on given full name and domain name, script asks for MX record (preferred mailserver). When we know "who to ask", we connect via telnet to SMTP server, which should usually listen on port 25, then it starts specified "talks" with SMTP to reveal if recipient exists. Tada.
# Will it always work?
Of course not. Servers are diffrent. Like people. Some wants to talk, some talks gibberish, some... just exists.
# Is this aggressive solution?
Not really, this script does not asks server unforgiving times per second (yet), check the code.
# Can we do it better?
I don't know yet. Let me know.
# How to use?
 1. Create .csv file named "VIP.csv", at first column provide full name (ex. Edward Fir) and in a second a domain name of service (ex. firfurs.com). You can provide as many entries as you want (I guess).

```
    Edward Fir;firfus.com
```

```
    Meg Megan;firfus.com
```

```
    Adam Madam;ponynop.com
```
 
 2. Check config.ini if it suits your preferences, their names are (almost) self explanatory.
 3. Fulfill dependencies based on requirements.txt file (via pip install), virtualenv recommended.
 3. Run smtp-crawler.py in a python
 4. Wait for output in Results.csv file
 5. Check which servers has banned your IP (jk).
 