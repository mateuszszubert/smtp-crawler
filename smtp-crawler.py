#!/bin/bash
# -*- coding: utf-8 -*-

# This program crawls specified SMTP servers using telnet connection
# and with specified commands it asks about existence of mail
# addresses based on algorithm.
#

from deps.Person import Person
from deps.TelnetHandler import TelnetHandler
from deps.DNSResolver import DNSResolver
from deps.SmtpCrawler import SmtpCrawler
from ConfigParser import ConfigParser
import csv, io, logging

def read_vips_from_csv(csv_path):
    VIPs = []
    with open(csv_path, "rb") as vipfile:
        vipreader = csv.reader(vipfile, delimiter=";")
        for row in vipreader:
            try:
                fullname = row[0].decode('windows-1250')
                name = fullname.split(" ")[0]
                surname = fullname.split(" ")[1]
                domain = row[1].decode('windows-1250')

                mail_from = None
                skip_helo_hi_validation = None
                helo_hi_validation_text = None

                try:
                    mail_from = row[2].decode('windows-1250')
                    skip_helo_hi_validation = row[3].decode('windows-1250')
                    helo_hi_validation_text= row[4].decode('windows-1250')
                except Exception:
                    pass

                log.debug(u"Checking MX info for domain {}".format(domain))
                mx_host = DNSResolver.ask_for_mxserver(domain)
                log.debug(u"MX IN {}".format(mx_host))

                vip = Person(fullname, name, surname, domain, mail_from, skip_helo_hi_validation, helo_hi_validation_text), TelnetHandler(mx_host, 25)
                VIPs.append(vip)
            except Exception, e:
                log.error(u"An exception has been catched during crawling (script will continue if possible)", exc_info=True)
    return VIPs

if __name__ == "__main__":
    
    # Reading app config    
    config = ConfigParser()
    config.read("config.ini")

    debug = config.getboolean("main","debug")
    logfile = config.get("main","logfile")

    vips_file = config.get("csv","input")
    output_file = config.get("csv","output")

    skip_hello_validation = config.getboolean("validation","skip_hello_validation")
    hello_regex = config.get("validation","hello_regex")
    from_regex = config.get("validation","from_regex")
    rcpt_regex = config.get("validation","rcpt_regex")
    ban_regex = config.get("validation","ban_regex")
    lying_detection_ratio = config.getfloat("validation", "lying_detection_ratio")
    invalidate_results_if_lying = config.getboolean("validation", "invalidate_results_if_lying")
    
    # Set logging
    log = logging.getLogger("-")
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)
    
    logformatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_filehandler = logging.FileHandler(logfile)
    log_consolehandler = logging.StreamHandler()
    
    log_filehandler.setFormatter(logformatter)
    log_consolehandler.setFormatter(logformatter)
    
    log.addHandler(log_filehandler)
    log.addHandler(log_consolehandler)

    log.info(u"App start.")
    log.debug(u"Debug logging: "+ str(debug))
    log.debug(u"Input file: "+vips_file)
    log.debug(u"Output file: "+output_file)
    log.debug(u"SMTP skip hello validation formula: "+str(skip_hello_validation))
    log.debug(u"SMTP hello validation formula: "+hello_regex)
    log.debug(u"SMTP from validation formula: "+from_regex)
    log.debug(u"SMTP rcpt validation formula: "+rcpt_regex)
    
    VIPs = read_vips_from_csv(vips_file)
    VIPs_len = len(VIPs)
    log.info(u"Read {len} person(s) data to explore.".format(len=VIPs_len))
    
    if VIPs_len > 0:
        log.info(u"All output goes to {output_file} file.".format(output_file=output_file))
        iterator = 1
        try:
            with io.open(output_file, "w", encoding="utf-8-sig") as output:
                for person, telnet_handler in VIPs:
                    log.info(u"Step {idx}: Exploring data of {person_fullname}".format(idx=iterator, person_fullname=person.fullname))

                    crawler = SmtpCrawler(person, telnet_handler, hello_regex, from_regex, rcpt_regex, ban_regex, lying_detection_ratio, invalidate_results_if_lying, skip_hello_validation)
                    accepted_mails = u", ".join(crawler.query_mails())
                    full_output = u"{};{}\n".format(person.fullname, accepted_mails)
                    
                    w = csv.writer(output)
                    output.write(full_output)
                    iterator = iterator + 1
        except Exception, e:
            log.error(u"An exception has been catched during crawling (script will continue if possible)", exc_info=True)
        log.info(u"All persons have been checked in a given criteria.")
    else:
        log.warn(u"Check input data in VIP.csv file.")

    log.info(u"App stop.")
