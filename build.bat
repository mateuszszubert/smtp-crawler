@echo off
rd /s /q .\build
rd /s /q .\dist
pyinstaller --onefile --hidden-import=trans,dnspython --paths=.\virtualenv\Lib\site-packages .\smtp-crawler.py
copy .\VIP.csv .\dist\VIP.csv
copy .\config.ini .\dist\config.ini
start .\dist