# -*- coding: utf-8 -*-
import dns.resolver

class DNSResolver(object):
    @staticmethod
    def ask_for_mxserver(domain):
        mx_responses = dns.resolver.query(domain, "MX")
        mx_servers = []
        for mx_server in mx_responses:
            mx_servers.append(unicode(mx_server))
        return mx_servers[0].split(" ")[1][:-1].strip()