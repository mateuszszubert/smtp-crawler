# -*- coding: utf-8 -*-
import telnetlib

class TelnetHandler(object):
    def __init__(self, host, port, timeout=5):
        self.timeout = timeout
        self.host = host
        self.port = port
        self.is_connected = False
    
    def connect(self):
        self.telnet_connection = telnetlib.Telnet()
        self.telnet_connection.open(host=self.host, port=self.port, timeout=self.timeout)
        # read motd/hello
        self.telnet_connection.read_until("\n", 5)
        self.is_connected = True

    def send_query(self, query):
        if self.is_connected:
            self.telnet_connection.write("{}\r\n".format(query))
            # wow!
            buffer = self.telnet_connection.read_until("\n", 5)
            response = buffer.strip()
            return response
        else:
            raise Exception("Connection with host {}:{} not established.".format(self.host, self.port))
        
    def close_connection(self):
        self.telnet_connection.close()
        self.is_connected = False