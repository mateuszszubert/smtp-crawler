# -*- coding: utf-8 -*-
from trans import trans
import unicodedata

class Person(object):
    def __init__(self, fullname, name, surname, domain, mail_from=None, skip_helo_hi_validation=None, helo_hi_validation_text=None):
        self.fullname = fullname
        self.name = name
        self.surname = surname
        self.domain = domain
        self.mail_from = mail_from
        self.skip_helo_hi_validation = skip_helo_hi_validation
        self.helo_hi_validation_text =  helo_hi_validation_text
    
    def normalize_str(self, text):
        return text.lower().encode("trans")

    def generate_mail_variants(self):
        variants = []

        name = self.normalize_str(self.name)
        surname = self.normalize_str(self.surname)
        domain = self.normalize_str(self.domain)

        template = u"{}{}{}@{}"
        formats = [
            [
                # ejodelko@kogucik.pl
                name[0], "", surname, domain
            ],
            [
                # e.jodelko@kogucik.pl
                name[0], ".", surname, domain
            ],
            [
                # edward.jodelko@kogucik.pl
                name, ".", surname, domain
            ],
            [
                # edward-jodelko@kogucik.pl
                name, "-", surname, domain
            ],
            [
                # edwardjodelko@kogucik.pl
                name, "", surname, domain
            ],
            [
                # jodelko.edward@kogucik.pl
                surname, ".", name, domain
            ],
            [
                # jodelko@kogucik.pl
                "", "", surname, domain
            ],
            [
                # jodelkoedward@kogucik.pl
                surname, "", name, domain
            ],
            [
                # edwardj@kogucik.pl
                name, "", surname[0], domain
            ],
            [
                # edward.j@kogucik.pl
                name, ".", surname[0], domain
            ],
            [
                # j.edward@kogucik.pl
                surname[0], ".", name, domain
            ],
            [
                # j.edward@kogucik.pl
                surname[0], ".", name, domain
            ],
            [
                # jedward@kogucik.pl
                surname[0], "", name, domain
            ],
            [
                # edward@kogucik.pl
                name, "", "", domain
            ]
        ]

        for format in formats:
            variants.append(template.format(*format))
        return variants