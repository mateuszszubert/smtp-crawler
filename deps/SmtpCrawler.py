# -*- coding: utf-8 -*-
# from deps.Person import Person
# from deps.TelnetHandler import TelnetHandler

import re, logging, time

class SmtpCrawler(object):
    def __init__(self, person, telnet_handler, hello_regex, mailfrom_regex, mailto_regex, ban_regex, lying_detection_ratio, invalidate_results_if_lying, skip_hello_validation):
        self.person = person
        self.telnet_handler = telnet_handler
        self.hello_regex = hello_regex
        self.mailfrom_regex = mailfrom_regex
        self.mailto_regex = mailto_regex
        self.ban_regex = ban_regex
        self.log = logging.getLogger("-")
        self.skip_hello_validation = skip_hello_validation
        self.lying_detection_ratio = lying_detection_ratio
        self.confirmed_mails = 0
        self.total_mails = 0
        self.invalidate_results_if_lying = invalidate_results_if_lying

        self.helo_hi_validation_text = self.person.helo_hi_validation_text if self.person.helo_hi_validation_text is not None else "helo hi"
        self.skip_hello_validation = self.person.skip_helo_hi_validation == "True" if self.person.skip_helo_hi_validation is not None else skip_hello_validation
        self.asking_mailaddress = self.person.mail_from if self.person.mail_from is not None else "{generic}@{domain}".format(generic="admin",domain=person.domain)

        self.log.debug("Person setting: Asking mail address: {}".format(self.asking_mailaddress))
        self.log.debug("Person setting: Skip helo hi validation: {}".format(self.skip_hello_validation))
        self.log.debug("Person setting: Helo hi validation text: {}".format(self.helo_hi_validation_text))
        
    def validation_hello_acceptation_level(self, text):
        # print re.findall(self.hello_regex, text)
        text = text.lower()
        return True if len(re.findall(self.hello_regex, text)) > 0 else False
    
    def validation_mailfrom_acceptation_level(self, text):
        # print re.findall(self.mailfrom_regex, text)
        text = text.lower()
        return True if len(re.findall(self.mailfrom_regex, text)) > 0 else False
    
    def validation_mailto_acceptation_level(self, text):
        text = text.lower()
        return True if len(re.findall(self.mailto_regex, text)) > 0 else False
    
    def validation_sender_banned(self, text):
        text = text.lower()
        return True if len(re.findall(self.ban_regex, text)) > 0 else False

    def get_efficiency_ratio(self):
        return float(self.confirmed_mails) / float(self.total_mails)
    
    def validate_mail(self, rcpt_target):
        self.telnet_handler.connect()
        hello_response = self.telnet_handler.send_query("helo hi")
        
        self.log.info(u"Checking mail {rcpt_target}".format(rcpt_target=rcpt_target))
        self.log.debug(u"Validation step #1: " + hello_response)
        
        if self.validation_hello_acceptation_level(hello_response) or self.skip_hello_validation:
            mailfrom_reponse = self.telnet_handler.send_query("mail from: <{}>".format(self.asking_mailaddress))
            is_host_banned = self.validation_sender_banned(mailfrom_reponse)

            if is_host_banned:
                self.log.warning(u"SMTP server claims that your host is blacklisted.\n{}".format(mailfrom_reponse))

            self.log.debug(u"Validation step #2: " + mailfrom_reponse)
            
            if self.validation_mailfrom_acceptation_level(mailfrom_reponse):
                mailto_response = self.telnet_handler.send_query("rcpt to: <{rcpt_target}>".format(rcpt_target=rcpt_target))
                self.log.debug(u"Validation step #3: " + mailto_response)
                
                if self.validation_mailto_acceptation_level(mailto_response):
                    self.telnet_handler.close_connection()
                    self.log.info(u"Found mail address: " + rcpt_target)
                    self.confirmed_mails += 1
                    return True

        self.telnet_handler.close_connection()
        return False
    
    def query_mails(self):
        accepted_mails = []
        person_possible_mails = self.person.generate_mail_variants()
        self.total_mails = len(person_possible_mails)
        self.log.debug("{} mails to check.".format(self.total_mails))
        for mail in person_possible_mails:
            if self.validate_mail(mail):
                accepted_mails.append(mail)
            # This sleep time should prevent from too high artificial traffic to target server
            time.sleep(0.05)

        efficiency_ratio = self.get_efficiency_ratio()
        if efficiency_ratio >= self.lying_detection_ratio:
            self.log.warning("This SMTP server exceeded a minimum value of efficiency range (minimum: {}, crawl result: {}).".format(self.lying_detection_ratio, efficiency_ratio))
            self.log.warning("It looks like this server is lying about available mails.")
            if self.invalidate_results_if_lying:
                self.log.warning("Results of this crawl will be invalidated.")
                accepted_mails = []
                efficiency_ratio = 0.0
                self.confirmed_mails = 0
        self.log.info("Found {} mails.".format(self.confirmed_mails))
        self.log.info("Crawl efficiency ratio: {}".format(efficiency_ratio))
        return accepted_mails